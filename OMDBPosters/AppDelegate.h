//
//  AppDelegate.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/24/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

