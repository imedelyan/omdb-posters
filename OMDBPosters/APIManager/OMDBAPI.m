//
//  OMDBAPI.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "OMDBAPI.h"

@implementation OMDBAPI

#define Server @"https://www.omdbapi.com/"
#define APIkey @"a6e8484b"

+ (void)getInformationForMovie:(NSString *)movieTitle withCompletion:(APIManagerBlock)completion {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    movieTitle = [movieTitle stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *URL = [NSString stringWithFormat:@"%@?t=%@&apikey=%@", Server, movieTitle, APIkey];
    
    [manager GET:URL parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject) {
            completion(responseObject,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error) {
            completion(nil,error);
        }
    }];
}

@end
