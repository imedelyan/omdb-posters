//
//  SpinnerView.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/29/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "SpinnerView.h"

@implementation SpinnerView

- (void)startAnimating {
    [self setHidden:NO];
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat: -2*M_PI];
    animation.toValue = [NSNumber numberWithFloat: 0.0f];
    animation.duration = 2.0f;
    animation.repeatCount = INFINITY;
    animation.removedOnCompletion = NO;
    
    [self.layer addAnimation:animation forKey:@"spin"];
}

- (void)stopAnimating {
    [self setHidden:YES];
    
    [self.layer removeAnimationForKey:@"spin"];
}

@end
