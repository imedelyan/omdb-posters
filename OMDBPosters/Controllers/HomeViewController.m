//
//  HomeViewController.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/25/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "HomeViewController.h"
#import "MovieDetailedViewController.h"
#import "MovieDataBase.h"
#import "OMDBAPI.h"
#import "SpinnerView.h"
#import "SearchHistoryTableViewCell.h"

@interface HomeViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet SpinnerView *spinner;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) MovieModel *movie;
@property (nonatomic, strong) NSMutableArray *movieSearchResult;

@end

@implementation HomeViewController

@synthesize movie;
@synthesize movieSearchResult;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setting delegates
    self.searchField.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self.spinner setHidden:YES];
    self.movieSearchResult = [[NSMutableArray alloc] init];
    
    // adding gesture recognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)];
    [self.view addGestureRecognizer:tap];
    [tap setCancelsTouchesInView:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = @"Open Movie Database Posters";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button actions

- (IBAction)searchMovieAction:(id)sender {
    [self updateUIStarBlock];
    
    // getting movie from local database
    self.movie = [MovieDataBase getMovieforMovieTitle:self.searchField.text];
    
    if (!self.movie) {
        // getting movie from omdbapi
        [OMDBAPI getInformationForMovie:self.searchField.text withCompletion:^(id result, NSError *error) {
            if (!error) {
                if (result) {
                    if ([[result objectForKey:@"Response"] isEqualToString:@"True"]) {
                        
                        self.movie = [MovieModel modelWithData:result];
                        
                        if (![self.movie.posterURL isEqualToString:@"N/A"]) {
                            // downloading poster image
                            NSURL *url = [NSURL URLWithString:self.movie.posterURL];
                            NSURLSessionDownloadTask *task = [[NSURLSession sharedSession] downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (!error) {
                                        if (location) {
                                            self.movie.poster = [NSData dataWithContentsOfURL:location];
                                        }
                                        [self saveSearchHistory];
                                        [MovieDataBase saveMovie:self.movie];
                                        [self updateUIFinishBlock];
                                        [self performSegueWithIdentifier:@"MovieDetailed" sender:self];
                                    } else {
                                        NSLog(@"Error: %@ ", error.localizedDescription);
                                        [self updateUIFinishBlock];
                                        [self showAlertWithTitle:@"Error" andMessage:@"downloading poster image!"];
                                    }
                                });
                                
                            }];
                            [task resume];
                            
                        } else {
                            [self saveSearchHistory];
                            [MovieDataBase saveMovie:self.movie];
                            [self updateUIFinishBlock];
                            [self performSegueWithIdentifier:@"MovieDetailed" sender:self];
                        }
                    } else {
                        [self updateUIFinishBlock];
                        [self showAlertWithTitle:@"Movie not found!" andMessage:nil];
                    }
                }
            } else {
                NSLog(@"Error: %@ ", error.localizedDescription);
                [self updateUIFinishBlock];
                [self showAlertWithTitle:@"Error" andMessage:@"requesting server API!"];
            }
        }];
    } else {
        [self saveSearchHistory];
        [self updateUIFinishBlock];
        [self performSegueWithIdentifier:@"MovieDetailed" sender:self];
    }
}

#pragma mark - Helpers

- (void)updateUIStarBlock {
    [self.spinner startAnimating];
    [self.view endEditing:YES];
    
    [self.searchField setUserInteractionEnabled:NO];
    [self.searchButton setUserInteractionEnabled:NO];
    [self.tableView setUserInteractionEnabled:NO];
}

- (void)updateUIFinishBlock {
    [self.spinner stopAnimating];
    self.searchField.text = @"";
    [self.movieSearchResult removeAllObjects];
    [self.tableView reloadData];
    
    [self.searchField setUserInteractionEnabled:YES];
    [self.searchButton setUserInteractionEnabled:YES];
    [self.tableView setUserInteractionEnabled:YES];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)saveSearchHistory {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (defaults) {
        NSMutableArray *searchHistory = [NSMutableArray arrayWithArray:[defaults objectForKey:@"searchHistory"]];
        [searchHistory insertObject:self.movie.title atIndex:0];
        [defaults setObject:searchHistory forKey:@"searchHistory"];
        [defaults synchronize];
    }
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.movieSearchResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
    
    if (cell == nil) {
        cell = [[SearchHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SearchHistoryTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MovieModel *movie = [self.movieSearchResult objectAtIndex:indexPath.row];
    
    // setting small poster image
    UIImage *posterImage;
    if (movie.poster) {
        posterImage = [UIImage imageWithData:movie.poster];
    } else {
        posterImage = [UIImage imageNamed:@"noposter"];
    }
    UIImage *smallPosterImage = [self imageWithImage:posterImage convertToSize:CGSizeMake(27, 40)];
    
    cell.smallPoster.image = smallPosterImage;
    cell.movieTitle.text = movie.title;
    cell.releaseDate.text = movie.released;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.movie = [self.movieSearchResult objectAtIndex:indexPath.row];
    [self saveSearchHistory];
    [self performSegueWithIdentifier:@"MovieDetailed" sender:self];
}

# pragma mark - Searchbar delegates

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchMovieAction:nil];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // reloading array of movies that match entering text
    [self.movieSearchResult removeAllObjects];
    self.movieSearchResult = [MovieDataBase getMovieArrayforSearchText:searchText];
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"MovieDetailed"]) {
        MovieDetailedViewController *vc = [segue destinationViewController];
        vc.movie = self.movie;
    }
}

@end
