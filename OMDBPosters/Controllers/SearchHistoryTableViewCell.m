//
//  SearchHistoryTableViewCell.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "SearchHistoryTableViewCell.h"

@implementation SearchHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.movieTitle.adjustsFontSizeToFitWidth = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
