//
//  SearchHistoryTableViewController.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/25/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "SearchHistoryTableViewController.h"
#import "SearchHistoryTableViewCell.h"
#import "MovieDetailedViewController.h"
#import "MovieDataBase.h"
#import "MovieModel.h"

@interface SearchHistoryTableViewController ()

@property (nonatomic, strong) NSMutableArray *searchHistory;
@property (nonatomic, strong) MovieModel *selectedMovie;

@end

@implementation SearchHistoryTableViewController

@synthesize searchHistory;
@synthesize selectedMovie;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.searchHistory = [NSMutableArray arrayWithArray:[defaults objectForKey:@"searchHistory"]];
    [self.tableView reloadData];
    
    self.navigationController.navigationBar.topItem.title = @"Open Movie Database Posters";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchHistory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchHistoryTableViewCell"];
    
    if (cell == nil) {
        cell = [[SearchHistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SearchHistoryTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    MovieModel *movie = [MovieDataBase getMovieforMovieTitle:[self.searchHistory objectAtIndex:indexPath.row]];
    
    // setting small poster image
    UIImage *posterImage;
    if (movie.poster) {
        posterImage = [UIImage imageWithData:movie.poster];
    } else {
        posterImage = [UIImage imageNamed:@"noposter"];
    }
    UIImage *smallPosterImage = [self imageWithImage:posterImage convertToSize:CGSizeMake(27, 40)];
    
    cell.smallPoster.image = smallPosterImage;
    cell.movieTitle.text = movie.title;
    cell.releaseDate.text = movie.released;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *movieTitle = [self.searchHistory objectAtIndex:indexPath.row];
    self.selectedMovie = [MovieDataBase getMovieforMovieTitle:movieTitle];
    
    [self performSegueWithIdentifier:@"MovieDetailedStored" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"MovieDetailedStored"]) {
        MovieDetailedViewController *vc = [segue destinationViewController];
        vc.movie = self.selectedMovie;
    }
}

@end
