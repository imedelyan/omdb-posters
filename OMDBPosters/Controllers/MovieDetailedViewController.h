//
//  MovieDetailedViewController.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/25/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieModel.h"

@interface MovieDetailedViewController : UIViewController

@property (nonatomic, strong) MovieModel *movie;

@end
