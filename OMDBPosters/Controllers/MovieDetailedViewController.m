//
//  MovieDetailedViewController.m
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/25/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import "MovieDetailedViewController.h"

@interface MovieDetailedViewController ()

@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *releaseDate;
@property (weak, nonatomic) IBOutlet UIImageView *posterView;

@end

@implementation MovieDetailedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setting movie title label
    self.movieTitle.text = self.movie.title;
    self.movieTitle.adjustsFontSizeToFitWidth = NO;
    self.movieTitle.numberOfLines = 0;
    
    CGFloat fontSize = 30;
    while (fontSize > 0.0) {
        CGRect textRect = [self.movie.title boundingRectWithSize:CGSizeMake(self.movieTitle.frame.size.width, 10000)
                                                         options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                      attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Verdana" size:fontSize]}
                                                         context:nil];
        if (textRect.size.height <= self.movieTitle.frame.size.height) break;
        fontSize -= 1.0;
    }
    
    self.movieTitle.font = [UIFont fontWithName:@"Verdana" size:fontSize];
    
    // setting release date label
    self.releaseDate.text = self.movie.released;
    self.releaseDate.font = [UIFont fontWithName:@"Verdana" size:fontSize];
    
    // setting poster image
    self.posterView.contentMode = UIViewContentModeScaleAspectFit;
    self.posterView.clipsToBounds = YES;

    UIImage *image;
    if (self.movie.poster) {
        image = [UIImage imageWithData:self.movie.poster];
    } else {
        image = [UIImage imageNamed:@"noposter"];
    }
    UIImage *posterImage = [self imageWithImage:image convertToSize:CGSizeMake(300, 442)];
    [self.posterView setImage:posterImage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = @"Back";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

@end
