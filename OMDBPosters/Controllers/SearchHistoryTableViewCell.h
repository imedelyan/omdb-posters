//
//  SearchHistoryTableViewCell.h
//  OMDBPosters
//
//  Created by Igor Medelyan on 11/27/17.
//  Copyright © 2017 Igor Medelyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *releaseDate;
@property (weak, nonatomic) IBOutlet UIImageView *smallPoster;

@end
